const canvas = document.getElementById('jsCanvas');
let painting = false,
    filling = false;
const ctx = canvas.getContext('2d');
const colors = document.getElementsByClassName('jsColor');
//console.log(Array.from(colors));
const range = document.getElementById('jsRange');
const mode = document.getElementById('jsMode');
const INITIAL_COLOR = 'black';
const saveBtn = document.getElementById('jsSave');

const CANVAS_SIZE = 700;

ctx.fillStyle = 'white';
ctx.fillRect(0, 0, CANVAS_SIZE,CANVAS_SIZE);


canvas.height = CANVAS_SIZE;
canvas.width = CANVAS_SIZE;
ctx.fillStyle = INITIAL_COLOR;

ctx.lineWidth = 2.5;
ctx.strokeStyle = INITIAL_COLOR;

function startPainting() {
    painting = true;
}

function stopPainting(){
    painting = false;
}

function onMouseMove(event){
    x = event.offsetX;
    y = event.offsetY;
    console.log(x, y, painting);
    if (!painting) {
        ctx.beginPath();
        ctx.moveTo(x,y);
    } else {
        console.log('ЛИНИЯ');
        ctx.lineTo(x,y);
        ctx.stroke();
    }
}

function onMouseDawn(event) {
    //console.log(event);
    painting = true;
}

/*
function onMouseUp(event) {
    //console.log(event);
    stopPainting();
}
*/

function handleColorClick(event){
    const color = event.target.style.backgroundColor;
    ctx.strokeStyle = color;
    ctx.fillStyle = ctx.strokeStyle;
    console.log(color);
}

function handleModeClick(){
    if(filling === true) {
        filling = false;
        mode.innerText = 'Рисование';
    } else {
        filling = true;
        mode.innerText = 'Заливка';
    }
}

function handleCanvacClick(){
    if(filling){
        ctx.fillRect(0, 0, CANVAS_SIZE,CANVAS_SIZE);
    }

}

function handleCM(event){
    event.preventDefault();
}

function savePic(){
    const image = canvas.toDataURL();
    //console.log(image);
    const link = document.createElement('a');
    link.href = image;
    link.download = 'fileName';
    //console.log(link);
    link.click();

}

if (canvas){
    canvas.addEventListener('mousemove', onMouseMove);
    canvas.addEventListener('mousedown', onMouseDawn);
    canvas.addEventListener('mouseup', stopPainting);
    canvas.addEventListener('mouseleave', stopPainting)
    canvas.addEventListener('click', handleCanvacClick);
    canvas.addEventListener('contextmenu', handleCM);
}

Array.from(colors).forEach(colors => colors.addEventListener('click', handleColorClick))

if (range){
    range.addEventListener('input', handleRangeChange);
}

function handleRangeChange(event){
    console.log(event.target.value);
    const rangeValue = event.target.value;
    ctx.lineWidth = rangeValue;
}

if (mode){
    mode.addEventListener('click', handleModeClick);
}

if (saveBtn){
    saveBtn.addEventListener('click', savePic);
}